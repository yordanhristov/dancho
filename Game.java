package dancho;
import java.util.Scanner;
class Hunter {
    private double health;
    private double damage;
    private int level;
    public int posx;
    public int posy;
    
    static void instructions() {
    	
        System.out.println("Izbirate si edin ot variantite za geroi i zapochvate igrata kato se izpravqte sreshtu protivnici edin po edin");
    }
    
    void attack(double enemyHealth) {
        enemyHealth -= damage;
        if(enemyHealth <= 0) {
            System.out.println("Victory");
        }else{
            System.out.println("Not enough damage");
        }
    }
    
    void guard(double enemyDamage) {
        health -= enemyDamage;
        if(health <= 0) {
            System.out.println("Defeat");
        }else{
            System.out.println("Not enough damage");
        }
    }
    
    double healthRegen(double heal) {
        health += heal;
        return health;
    }
    
    Hunter(double health, double damage, int level, int x, int y) {
        this.health = health;
        this.damage = damage;
        this.level = level;
        setPosx(x);
        setPosy(y);
    }
    
    public int getPosx() {
        return posx;
    }
    
    public void setPosx(int posx) {
        this.posx = posx;
    }
    
    public int getPosy() {
        return posy;
    }
    
    public void setPosy(int posy) {
        this.posy=posy;
    }
}


class Map {
    private int sizex;
    private int sizey;
    public Hunter Survivor;
    
    public Map(int x, int y) {
        sizex = x;
        sizey = y;
    }
    
    public void setHunter(Hunter Survivor) {
        this.Survivor = Survivor;
    }
    
    void print() {
        for (int i = 0; i < sizey; i++) {
            for (int j = 0; j < sizex; j++) {
                if (i == Survivor.getPosy() && j == Survivor.getPosx()) {
                    System.out.print("H ");
                }else{
                    System.out.print(". ");
                }
            }
            System.out.println();
        }
    }
}

public class Game {
    
    public static void main(String args[]) {
    	Scanner scan = new Scanner(System.in);
        Map m = new Map(9, 5);
        Hunter Marksman = new Hunter(250, 75, 1, 1, 1);
        Hunter Survivor = new Hunter(350, 55, 1, 1 ,2);
        
        Hunter.instructions();
        System.out.println("Napishete M za Marksman ili S za Survivor");
        char c = scan.next().charAt(0);
        if(c=='m' || c=='M')
        {
        	m.setHunter(Marksman);
        	System.out.println("Marksman selected");
        }
        if(c=='s' || c=='S')
        {
        	m.setHunter(Survivor);
        	System.out.println("Survivor selected");
        }
       
        while(true) {
            m.print();
            char posoka = scan.next().charAt(0);
            switch(posoka) {
                case 'w' : 
                	{
                		if(m.Survivor.posy-1 >= 0)
                		{
                		m.Survivor.posy--; 
                		}
                		break;
                	}
                case 's' : 
                	{
                		if(m.Survivor.posy+1 < 5)
                		{
                		m.Survivor.posy++;
                		}
                		break;
                	}
                case 'a' :
                	{
                		if(m.Survivor.posx-1 >= 0)
                		{
                		m.Survivor.posx--; 
                		}
                		break;
                	}
                case 'd' : 
                    {
            		    if(m.Survivor.posx+1 < 9)
            		    {
            			m.Survivor.posx++; 
            		    }
            		    break;
                    }
            }
            
        }
    }
}
